# 全新·启航————PTB+(PyToolBox+)

tip:源码在[PTStudio/GUI](https://gitee.com/ptstudio/gui)

![输入图片说明](https://images.gitee.com/uploads/images/2021/0921/153818_1ea18f55_8381940.png "log.png")

#### 什么是PTB+？

可以理解为py-dos的GUI版本,不过经过了重新编写、设计

具体使用方法见WiKi!

待完善......

下载方法：
1.下载压缩包
    点击仓库"统计"按钮，找到"发行版"
    下载"ptb .zip"
2.pip安装
    命令行运行"pip install ptb"

暂未提供Linux / macOS版本，正在测试兼容性!